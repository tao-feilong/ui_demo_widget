using Microsoft.AspNetCore.Mvc;
using System;
using System.Net.Http;
using System.Text.RegularExpressions;
using PexelsDotNetSDK.Api;
using System.Text;

namespace test1.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ImageController : ControllerBase
    {
        private readonly HttpClient _httpClient;

        public ImageController(IHttpClientFactory httpClientFactory)
        {
            var httpClientHandler = new HttpClientHandler();
            // 禁用 SSL 证书验证
            httpClientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => true;

            _httpClient = new HttpClient(httpClientHandler);
        }




        [HttpGet("search")]
        public async Task<IActionResult> search()
        {
            try
            {
                string parameterValue = HttpContext.Request.Query["query"];
                if(parameterValue == null) { parameterValue = "pig"; }

                List<string> ans = new List<string>();

                PexelsClient pexelsClient = new PexelsClient("2hEADr7lEzaVaa4teEAUxCqa6f8iD5l358gKbv1sKeh89A7mpdRHucsv");
                PexelsDotNetSDK.Models.PhotoPage result = await pexelsClient.SearchPhotosAsync(parameterValue, "portrait");
                for(int i=0;i<result.photos.Count;i++)
                {
                    PexelsDotNetSDK.Models.Photo photo = result.photos[i];
                    string str = photo.source.large;
                    ans.Add(str);
                }
                Random random = new Random();
                int randomNumber = random.Next(0, 15);

                string title = randomNumber % 2 == 0 ? "黄明是弟弟" : "魏强是弟弟";

                string a = $"<title>{title}</title><img alt=\"\" src=\"{ans[randomNumber]}\" />";

                // 将转发响应内容返回给调用方
                return Content(a, "text/html");
                

            }
            catch (Exception ex)
            {
                // 处理异常情况
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("home")]
        public IActionResult Forward1()
        {
            var htmlContent = new StringBuilder();
            htmlContent.AppendLine("<!DOCTYPE html>");
            htmlContent.AppendLine("<html lang='zh-CN'>");
            htmlContent.AppendLine("<head>");
            htmlContent.AppendLine("<meta charset='UTF-8'>");
            htmlContent.AppendLine("<meta name='viewport' content='width=device-width, initial-scale=1.0'>");
            htmlContent.AppendLine("<title>首页</title>");
            // 内联CSS
            htmlContent.AppendLine("<style>");
            htmlContent.AppendLine("  body {");
            htmlContent.AppendLine("    font-family: 'Arial', sans-serif;");
            htmlContent.AppendLine("    margin: 0;");
            htmlContent.AppendLine("    padding: 0;");
            htmlContent.AppendLine("    background: #f4f4f4;");
            htmlContent.AppendLine("    text-align: center;");
            htmlContent.AppendLine("  }");
            htmlContent.AppendLine("  h1 {");
            htmlContent.AppendLine("    color: #333;");
            htmlContent.AppendLine("  }");
            htmlContent.AppendLine("  .beian {");
            htmlContent.AppendLine("    position: fixed;");
            htmlContent.AppendLine("    bottom: 0;");
            htmlContent.AppendLine("    width: 100%;");
            htmlContent.AppendLine("    background: #222;");
            htmlContent.AppendLine("    color: #fff;");
            htmlContent.AppendLine("    text-align: center;");
            htmlContent.AppendLine("    padding: 10px 0;");
            htmlContent.AppendLine("  }");
            htmlContent.AppendLine("  .beian a {");
            htmlContent.AppendLine("    color: #40E0D0;");
            htmlContent.AppendLine("    text-decoration: none;");
            htmlContent.AppendLine("  }");
            htmlContent.AppendLine("</style>");
            htmlContent.AppendLine("</head>");
            htmlContent.AppendLine("<body>");
            htmlContent.AppendLine("<h1>欢迎来到emo大王的小窝</h1>");
            htmlContent.AppendLine("<p>这是属于我们的</p>");
            // 悬挂备案号的部分
            htmlContent.AppendLine("<div class='beian'>");
            htmlContent.AppendLine("<a href='https://beian.miit.gov.cn' target='_blank'>备案号：晋ICP备2024033534号</a>");
            htmlContent.AppendLine("</div>");
            htmlContent.AppendLine("</body>");
            htmlContent.AppendLine("</html>");

            return Content(htmlContent.ToString(), "text/html", Encoding.UTF8);

        }



        [HttpGet]
        public async Task<IActionResult> Forward()
        {
            try
            {
                // 构造转发请求
                var forwardRequest = new HttpRequestMessage(HttpMethod.Get, "http://img.xjh.me/random_img.php");

/*                // 复制原始请求的头部、正文等信息到转发请求中
                foreach (var header in Request.Headers)
                {
                    forwardRequest.Headers.TryAddWithoutValidation(header.Key, header.Value.ToArray());
                }
                forwardRequest.Content = new StreamContent(Request.Body);*/

                // 发送转发请求并获取响应
                var forwardResponse = await _httpClient.SendAsync(forwardRequest);

                // 获取转发响应的内容
                var responseContent = await forwardResponse.Content.ReadAsStringAsync();
                //responseContent.Replace("<title>岁月小筑随机图片API接口-随机背景图片-随机图片API</title>", "胡耀伟是shit");

                responseContent = Regex.Replace(responseContent, @"<title>.*</title>", "<title>胡耀伟是shit</title>");

                // 复制转发响应的头部、状态码到原始响应中
                foreach (var header in forwardResponse.Headers)
                {
                    Response.Headers[header.Key] = header.Value.ToArray();
                }
                Response.StatusCode = (int)forwardResponse.StatusCode;

                // 将转发响应内容返回给调用方
                return Content(responseContent, forwardResponse.Content.Headers.ContentType.MediaType);
            }
            catch (Exception ex)
            {
                // 处理异常情况
                return BadRequest(ex.Message);
            }
        }
    }
}
